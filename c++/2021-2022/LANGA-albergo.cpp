//Problema: Albergo 
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 26/04/2022

#include <iostream>
using namespace std;

main()
{
    char r;
    do 
    {
        int n,s=0,d=0,q=0;
        do
        {
            cout<<"inserisci il numero delle camere: ";
            cin>>n;	
        } while (n<2);
        
        int v[n];
        for (int i=0; i<n; i++)
            {
                do {
                       cout<<"numero di letti della camera "<<i<<" (1,2,4): ";
                       cin>>v[i]; 
                   } while (v[i]!=1 && v[i]!=2 && v[i]!=4);
            }

        for (int i=0; i<n; i++)
            {
                switch (v[i])
                {
                case 4:
                    q++;
                    break; 
                case 2:
                    d++;
                    break; 
                case 1:
                    s++;
                    break; 
                }
            }
        cout<<"numero di camere-->\tsignole:"<<s<<"\tdoppie:"<<d<<"\tquadruple:"<<q<<endl;

        cout<<"Vuoi ricominciare? (s/n): ";
        cin>>r;
    } while (r=='s');
}
