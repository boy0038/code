//Problema: biglietto_museo 
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 19/12/2021
#include <iostream>
using namespace std;

main()
{
    int n;
    string r;
    cout<<"Opzione 1--> Ingresso per bambini di eta' inferiore a 6 anni."<<endl;
    cout<<"Opzione 2--> Ingresso per gli studenti."<<endl;
    cout<<"Opzione 3--> Ingresso per i pensionati."<<endl;
    cout<<"Opzione 4--> Ingresso per tutti gli altri."<<endl;
    cout<<""<<endl; //ho lasciato una linea vuota in modo da dividere le opzioni dalla domanda per chiarezza.
    do {  
    cout<<"Inserisci l'opzione desiderata: ";
    cin>>n;
    switch (n)
    {
    case 1:
        cout<<"L'ingresso per i bambini di eta' inferiore a 6 anni e' gratuito."<<endl;
        break;
    case 2:
        cout<<"Il costo per gli studenti e' di 8 Euro."<<endl;
       break;
    case 3:
        cout<<"Il costo per i pensionati e' di 10 Euro."<<endl;
        break;
    case 4:
        cout<<"Il costo per tutti gli altri e' di 15 Euro."<<endl;
        break;
    default:
        cout<<"Il numero dell'opzione inserita non e' valida."<<endl;
        break;
    }
    cout<<"Vuoi ricominciare? S(i)/N(o): ";
    cin>>r;
       } while (r=="s" || r=="si" || r=="S" || r=="SI");
}
