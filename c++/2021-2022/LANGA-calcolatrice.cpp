//Problema: Calcolatrice 
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 04/12/2021
#include <iostream>
using namespace std;

main()
{
    char s;
    float a,b;
    cout<<"Inserisci il primo numero: ";
    cin>>a;
    cout<<"Inserisci il secondo numero: ";
    cin>>b;
    cout<<"Quale operazione si desidera fare? ( /, *, -, + ) ";
    cin>>s;
    switch (s)
    {
    case '+':
        cout<<"La somma e' uguale a "<<a+b;
        break;
    case '-':
        cout<<"La differenza e' uguale a "<<a-b;
        break;
    case '/':
        cout<<"Il quoziente e' uguale a "<<a/b;
        break;
    case '*':
        cout<<"Il prodotto e' uguale a "<<a*b;
        break;
    default:
        cout<<"L'operazione aritmetica scelta e' errata";
        break;
    }
}
