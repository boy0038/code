//Problema: es2-scheda_iterazione_testi_problemi
//Compilatore: Dev-C++ 5.11
//Autore: Laerti Langa
//Data di creazione: 09/02/2022
#include <iostream>
using namespace std;

main()
{
	int num,p;
    string r;
    do {
	cout<<"Inserire un numero: ";
    cin>>num;
	p=0;
	while (num!=0)
	{
        if (num%3==0)
            p++;
	    cout<<"Numero: ";
        cin>>num;
	}
    cout<<"Quantita' dei numeri multipli di 3:  "<<p<<endl;
    cout<<"Vuoi ricominciare? S(i)/N(o): ";
    cin>>r;
    } while (r=="s" || r=="si");
}
