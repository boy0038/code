//Problema: pro3-scheda_iterazione_testi_problemi
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 10/02/2022
#include <iostream>
using namespace std;

main()
{   
    int num,p;
    string r;
    do {
        cout<<"Inserire un numero: ";
        cin>>num;
        p=0;
    while (p<=num)
        {
            cout<<p<<endl;
            p++;
        }
        cout<<"Vuoi ricominciare? S(i)/N(o): ";
        cin>>r;
    } while (r=="s" || r=="si");
}
