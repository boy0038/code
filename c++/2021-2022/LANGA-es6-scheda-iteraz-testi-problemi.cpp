//Problema: pro6-scheda_iterazione_testi_problemi
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 16/02/2022
#include <iostream>
using namespace std;

main()
{
    int num,p,s,h;
    string r;
    do {
        cout<<"Inserire il numero di persone: ";
        cin>>num;
        p=0;
        s=0;
        while (p<num)
        {
            cout<<"inserire l'altezza:"<<endl;
            cin>>h;
            s=s+h;
            p++;
        }
        if (s/num>=170)
            cout<<"giganti"<<endl;
        else
            cout<<"bassotti"<<endl;
        cout<<"Vuoi ricominciare? S(i)/N(o): ";
        cin>>r;
    } while (r=="s" || r=="si");
}
