//Problema: Problema 7 pag 84
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 29/03/2022

#include <iostream> 
using namespace std;
main()
{
    int n,s=0;
    string x;
    char r;
    do {
        do {
            cout<<"inserisci la dimensione del vettore.. ";
            cin>>n;	
         } while (n<2);
        
        cout<<"inserire il valore da verificare: ";
        cin>>x;
        
        string v[n];
        
        for(int i=0;i<n;i++)
            {
                cout<<"v["<<i<<"]= ";
                cin>>v[i];
            }
        for (int i=0;i<n;i++)
                if (v[i]==x)
                    s++; 
        if (s>0)
            cout<<"Il valore \""<<x<<"\" compare "<<s<<" volta/e"<<endl;
        else
            cout<<"Il valore \""<<x<<"\" non e' presente"<<endl;

        cout<<"Vuoi ricominciare? S(i)/N(o): ";
        cin>>r;
     } while (r=='s');

}
	




