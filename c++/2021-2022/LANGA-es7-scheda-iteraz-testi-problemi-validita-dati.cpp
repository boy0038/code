//Problema: pro7-scheda_iterazione_testi_problemi (con controllo validità dati in ingresso)
//compilatore: Dev-C++ 5.7.1
//Autore: Laerti Langa
//Data di creazione: 04/03/2022 
#include <iostream>
using namespace std;

main()
{	int p,num;
    float s,v;
    char r;
    do {
        do {
            cout<<"Inserire il numero di voti: ";
            cin>>num;
        } while (num<=0);  
        p=0;
        s=0;
        while (p<num)
        {
            do {
                cout<<"inserire il voto: ";
                cin>>v;
            } while (v<0);
            s=s+v;
            p++;
        }
        cout<<"la media e': "<<s/num<<endl;
        if (s/num>=6)
            cout<<"Promosso"<<endl;
        else
            cout<<"Bocciato"<<endl;
        cout<<"Vuoi ricominciare? S(i)/N(o): ";
        cin>>r;
    } while (r=='s');
}
