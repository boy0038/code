//Problema: pro7-scheda_iterazione_testi_problemi
//compilatore: Dev-C++ 5.7.1
//Autore: Laerti Langa
//Data di creazione: 23/02/2022 (finito alle 10:15)
#include <iostream>
using namespace std;

main()
{	int p,num;
    float s,v;
    string r;
    do {
        cout<<"Inserire il numero di voti: ";
        cin>>num;
        p=0;
        s=0;
        while (p<num)
        {
            cout<<"inserire il voto:"<<endl;
            cin>>v;
            s=s+v;
            p++;
        }
        cout<<"la media e': "<<s/num<<endl;
        if (s/num>=6)
            cout<<"Promosso"<<endl;
        else
            cout<<"Bocciato"<<endl;
        cout<<"Vuoi ricominciare? S(i)/N(o): ";
        cin>>r;
    } while (r=="s" || r=="si");
}
