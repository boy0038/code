//Problema: pro8-scheda_iterazione_testi_problemi
//compilatore: Dev-C++ 5.7.1
//Autore: Laerti Langa
//Data di creazione: 01/03/2022
#include <iostream>
using namespace std;

main()
{   
    float num,m,s=0;
    char r;
    do {
        cout<<"Inserire il numero: ";
        cin>>num;
        cout<<"Inserire il valore massimo del multiplo: ";
        cin>>m;
        while (s<m)
        {
            cout<<"multiplo : "<<s<<endl;
            s=s+num;
        }
        s=0;
        cout<<"Vuoi ricominciare? S(i)/N(o): ";
        cin>>r;
    } while (r=='s');
}
