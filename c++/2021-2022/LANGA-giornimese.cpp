//Problema: GiorniMese 
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 04/12/2021
#include <iostream>
using namespace std;

main()
{
    int m,a;
    cout<<"Inserisci il numero del mese: ";
    cin>>m;
    switch (m)
    {
    case 1:
        cout<<"Il mese di Gennaio ha 31 giorni";
        break;
    case 2:
        cout<<"Inserisci l'anno: ";
        cin>>a;
        if (a%4==0 && a%100!=0)
            {
                cout<<"L'anno e' bisestile"<<endl;
                cout<<"Il mese di Febbraio ha 29 giorni";
            }
        else
            {
                if (a%400==0)
					{
                        cout<<"L'anno e' bisestile"<<endl;
                        cout<<"Il mese di Febbraio ha 29 giorni ";
					}
                else 
                    {
                        cout<<"L'anno non e' bisestile"<<endl;
                        cout<<"Il mese di Febbraio ha 28 giorni ";
                    }            
            }
        break;
    case 3:
        cout<<"Il mese di Marzo ha 31 giorni";
        break;
    case 4:
        cout<<"Il mese di Aprile ha 30 giorni";
        break;
    case 5:
        cout<<"Il mese di Maggio ha 31 giorni";
        break;
    case 6:
        cout<<"Il mese di Giugno ha 30 giorni";
        break;
    case 7:
        cout<<"Il mese di Luglio ha 31 giorni";
        break;
    case 8:
        cout<<"Il mese di Agosto ha 31 giorni";
        break;
    case 9:
        cout<<"Il mese di Settembre ha 30 giorni";
        break;
    case 10:
        cout<<"Il mese di Ottobre ha 31 giorni";
        break;
    case 11:
        cout<<"Il mese di Novembre ha 30 giorni";
        break;
    case 12:
        cout<<"Il mese di Dicembre ha 31 giorni";
        break;
    default:
        cout<<"Il numero inserito non appartiene ad un mese";
        break;
   }
}
