//Problema: impianto di risalita 
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 29/11/2021
#include <iostream>
using namespace std;

main()
{
    char s;
    cout<<"Inserisci la stagione: [(e)state / (a)utunno / (i)nverno / (p)rimavera] ";
    cin>>s;
    switch (s)
    {
    case 'e':
        cout<<"Il costo dell'impianto e' di "<<7+7*0.15<<" Euro";
        break;
    case 'a':
        cout<<"Il costo dell'impianto e' di "<<7*0.5<<" Euro";
        break;
    case 'i':
        cout<<"Il costo dell'impianto e' di "<<7*2<<" Euro";
        break;
    default:
        cout<<"Il costo dell'impianto e' di 7 Euro";
        break;
    }
}
