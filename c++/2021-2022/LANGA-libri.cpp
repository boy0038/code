//Problema: Libri 
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 26/04/2022

#include <iostream>
using namespace std;

main()
{
    char r;
    do 
    {
        int n,d=0,q=0;
        do
        {
            cout<<"inserisci il numero di libri: ";
            cin>>n;	
        } while (n<2);
        
        int v[n];
        for (int i=0; i<n; i++)
            {
                do
                {    
                    cout<<"inserire il prezzo di pagine per il libro numero "<<i<<": ";
                    cin>>v[i];
                } while (v[i]<0);
            }

        int pag;
        do
        {
            cout<<"inserisci il numero di pagine da verificare: ";
            cin>>pag;	
        } while (pag<=0);

        int c=0;
        for (int i=0; i<n; i++)
            {
                if (v[i]>=1200)
                    c++;
            }

        if (c>0)
            cout<<"numero di libri con almeno 1200 pagine: "<<c<<endl;
        else
            cout<<"non sono presenti libri con almeno 1200 pagine: "<<endl;

        cout<<"le posizioni dei libri con un numero di pagine maggiore di "<<pag<<" sono:"<<endl;
        for (int i=0; i<n; i++)
            {
                if (v[i]>=pag)
                    cout<<"-->" <<i<<endl;
            }

        int s=0;
        int conta_sup=0;

        for (int i=0; i<n; i++)
            {
                s=s+v[i];
            }
        float media = s/n;
        for (int i=0; i<n; i++)
            {
                if (v[i]>=media)
                        conta_sup++;
            }
        cout<<"i libri che hannno un numero di pagine sopra la media sono: "<<conta_sup<<endl;

        cout<<"Vuoi ricominciare? (s/n): ";
        cin>>r;
    } while (r=='s');
}
