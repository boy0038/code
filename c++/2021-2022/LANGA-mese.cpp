//Problema: mese
//Compilatore: Dev-C++
//Autore: Laerti Langa
//Data di creazione: 24/11/2021
#include <iostream>
using namespace std;

main()
{
    int n;
    do {
    cout<<"Inserire il numero del mese: ";
    cin>>n;
    } while (n<1 || n>13);
    switch (n)
    {
    case 1:
        cout<<"Siamo nel mese di Gennaio";
        break;
    case 2:
        cout<<"Siamo nel mese di Febbraio";
        break;
    case 3:
        cout<<"Siamo nel mese di Marzo";
        break;
    case 4:
        cout<<"Siamo nel mese di Aprile";
        break;
    case 5:
        cout<<"Siamo nel mese di Maggio";
        break;
    case 6:
        cout<<"Siamo nel mese di Giugno";
        break;
    case 7:
        cout<<"Siamo nel mese di Luglio";
        break;
    case 8:
        cout<<"Siamo nel mese di Agosto";
        break;
    case 9:
        cout<<"Siamo nel mese di Settembre";
        break;
    case 10:
        cout<<"Siamo nel mese di Ottobre";
        break;
    case 11:
        cout<<"Siamo nel mese di Novembre";
        break;
    case 12:
        cout<<"Siamo nel mese di Dicembre";
        break;
    }
}
