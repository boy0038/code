//Problema: Numeri_Fibonacci 
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 12/01/2022
#include <iostream>
using namespace std;

main()
{
    int n,a,b,c,conta;
    string r;
    do {
    cout<<"Inserire la quantita' di numeri da stampare: ";
    cin>>n;
    a=1;
    b=1;
    cout<<"n--->"<<a<<endl;
    cout<<"n--->"<<b<<endl;
    conta=2;
	while (conta<n)
	{
        c=a+b;
        cout<<"n--->"<<c<<endl;
        a=b;
        b=c;
        conta=conta+1;
	}
    cout<<"Vuoi ricominciare? S(i)/N(o): ";
    cin>>r;
    } while (r=="s" || r=="si" || r=="S" || r=="SI");
}
