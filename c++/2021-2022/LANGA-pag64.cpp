//Problema: Problema 7 pag 84
//Compilatore: Dev-C++ 5.11
//Autore: Laerti Langa
//Data di creazione: 13/04/2022

#include <iostream>
using namespace std;

main()
{
	int n,a=0,max=0;
	char r;
	
	do {
		cout<<"inserire la dimensione del vettore: ";
		cin>>n;
	} while (n<2);
	
	int v[n];
	
	for (int i=0;i<n;i++)
		{
                cout<<"v["<<i<<"]= ";
                cin>>v[i];
		}
		
	max=v[0];
	
	for (int i=0;i<n;i++)
		{
			if (v[i]>max)
				max=v[i];
		}
		cout<<max<<endl;

}
