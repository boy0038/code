//Problema: pedaggio autostradale
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 00/00/2022
#include <iostream>
using namespace std;
main()
{
    float km,t;
    char c;
    string r;
    do
    {
        cout<<"Inserisci il numero di chilometri percorsi: ";
        cin>>km;
        cout<<"Inserisci l'iniziale del codice tariffa (nord,centro,sud): ";
        cin>>c;
        switch (c)
        {
        case 'n':
            t=km*0.75;
            break;
        case 'c':
            t=km*0.6;
            break;
        case 's':
            t=km*0.5;
            break;
        default:
            cout<<"Il codice tariffa inserito e' errato.";
            break;
        }
        if (km>100)
            t=t+0.1*100+(km-100)*0.05;
        else
            t=t+km*0.1;
        cout<<"Il costo totale del pedaggio autostradale e' di "<<t<<" Euro."<<endl;
        cout<<"Vuoi ricominciare? S(i)/N(o): ";
        cin>>r;
    } while (r=="s" || r=="si" || r=="S" || r=="SI");
}