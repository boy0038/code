//Problema: Prezzi 
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 26/04/2022

#include <iostream>
using namespace std;

main()
{
	char r;
	do
	{
        int n,mag=0,c=0;
        do
        {
            cout<<"inserire il numero di prezzi: ";
            cin>>n;
        } while (n<2);

        float v[n];

        for (int i=0; i<n; i++)
            {
                do
                {    
                    cout<<"inserire il prezzo numero "<<i<<": ";
                    cin>>v[i];
                } while (v[i]<0);
            }

        for (int i=0; i<n; i++)
            {
                if (v[i]>mag)
                    mag=v[i];
            }
            
        float min=mag;
        for (int i=0; i<n; i++)
            {
                if (v[i]<min)
                    min=v[i];
            }
        float med=0;
        for (int i=0; i<n; i++)
            {
                    med=med+v[i];
            }

        float q[n];
        for (int i=0; i<n; i++)
            {
                q[i]=v[i]-v[i]*0.05;
            }
    
        for (int i=0; i<n; i++)
            {
                if (q[i]<=100)
                    c++;
            }
    
        cout<<"il prezzo medio  e': "<<med/n<<endl;
        cout<<"il prezzo massimo  e': "<<mag<<endl;
        cout<<"il prezzo minimo  e': "<<min<<endl;

        cout<<"vettore con prezzi scontati del 5%:"<<endl;
        for (int i=0; i<n; i++)
            {
                cout<<"prezzo "<<i<<" scontato: "<<q[i]<<endl;
            }

        cout<<"il numero di prezzi scontati minori di 100 e': "<<c<<endl;

        cout<<"vuoi ripetere il programma? (s/n): ";
        cin>>r;
    } while (r=='s');
    
}
