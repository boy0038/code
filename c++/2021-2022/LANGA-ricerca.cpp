//Problema: ricerca 
//Compilatore: 
//Autore: Laerti Langa
//Data di creazione: 17/05/2022
#include <iostream>
using namespace std;
main()
{
	char r;
	do{
		int n;
        do{
            cout<<"inserire la grandezza del vettore: ";
            cin>>n;
           } while (n<2);

            int v[n];
        
            for (int i=0; i<n; i++)
            {
                cout<<"inserire il valore numero "<<i<<": ";
                cin>>v[i];
            }

            int x;
            cout<<"inserire il valore da cercare: ";
            cin>>x;
            
            int c=0; //contatore

            for (int i=0; i<n; i++)
                {
                    if (v[i]==x)
                        {
                            cout<<"Il valore si trova nella posizione numero "<<i<<endl;
                            c++;
                        }
                }

            if (c>0)
                cout<<"Numero di volte il cui il valore si presenta: "<<c<<endl;
            else
                cout<<"Elemento non trovato"<<endl;

		cout<<"Vuoi ripetere il programma? (s/n)";
		cin>>r;
	} while (r=='s');
}

