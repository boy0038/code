//Problema: Somma_Media
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 12/01/2022
#include <iostream>
using namespace std;

main()
{
	float num,somma,conta,media;
    string r;
    do {
	cout<<"Inserire un numero: ";
    cin>>num;
	somma=0;
	conta=0;
	while (num!=0)
	{
		somma=somma+num;
		conta=conta+1;
		cout<<"Numero:";
		cin>>num;
	}
	media=somma/conta;
	cout<<"La somma e' "<<somma<<endl;
	cout<<"La media e' "<<media<<endl;
    cout<<"Vuoi ricominciare? S(i)/N(o): ";
    cin>>r;
    } while (r=="s" || r=="si" || r=="S" || r=="SI");
}

