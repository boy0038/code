//Problema: SommaPari_SommaDispari
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 15/01/2022
#include <iostream>
using namespace std;

main()
{
	int num,somma,d,p,r;
    string z;
    do {
	cout<<"Inserire un numero: ";
    cin>>num;
	p=0;
	d=0;
	while (num!=0)
	{
        r=num%2;
        if (r==0)
            p=p+num;
        else
            d=d+num;
	    cout<<"Numero: ";
        cin>>num;
	}
    cout<<"Somma dei numeri pari: "<<p<<endl;
    cout<<"Somma dei numeri dispari: "<<d<<endl;
    cout<<"Vuoi ricominciare? S(i)/N(o): ";
    cin>>z;
    } while (z=="s" || z=="si" || z=="S" || z=="SI");
}
