//Problema: Libri 
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 02/05/2022
#include <iostream>
using namespace std;
main ()
{
    char r;
    do {
        int n,sec,pen,sos=0,mul=0;
        
        do {
            cout<<"inserire grandezza vettore:";
            cin>>n;
        } while (n<2);

        int v[n];

        for (int i=0; i<n; i++)
            {
                cout<<"v["<<i<<"]: ";
                cin>>v[i];
            }
        sec=v[1];
        pen=v[n-2];
        v[1]=pen;
        v[n-2]=sec;

        cout<<endl<<"vettore dopo aver scambiato il secondo valore col penultimo:"<<endl;

        for (int i=0; i<n; i++)
            cout<<"v["<<i<<"]: "<<v[i]<<endl;

        cout<<endl<<"vettore dopo aver sostituito i valori negativi con gli opposti:"<<endl;

        for (int i=0; i<n; i++)
            {
                if (v[i]<0)
                    {
                        v[i]=v[i]*-1;
                        sos++;
                        cout<<"v["<<i<<"]: "<<v[i]<<endl;
                    }
                else
                    cout<<"v["<<i<<"]: "<<v[i]<<endl;
            }
        cout<<endl<<"numero di sostituzioni eseguite: "<<sos<<endl;

        for (int i=0; i<n; i++)
            {
                if (v[i]%3==0)
                    mul++;
            }
        if (mul>0)
            cout<<"gli elementi multipli di 3 sono "<<mul<<endl;
        else
            cout<<"nessun elemento soddisfa la richiesta"<<endl;
         
        for (int i=n-1; i>n-4; i--)
            v[i]=v[i]*3;
 
        cout<<endl<<"vettore dopo aver triplicato gli ultimi 3 valori:"<<endl;

        for (int i=0; i<n; i++)
            cout<<"v["<<i<<"]: "<<v[i]<<endl;

        cout<<"vuoi ricominciare? (s/n)";
        cin>>r;
       } while (r=='s');
}            
