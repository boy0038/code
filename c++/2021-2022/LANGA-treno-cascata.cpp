//Problema: treno (selezione a cascata)
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 16/11/2021

#include <iostream>
using namespace std;

main()
{
    int biglietto;
    float suppl,bigliettoTot;
    string tipo;
    cout<<"Inseririsci il prezzo del biglietto: ";
    cin>>biglietto;
    cout<<"Inseririsci il tipo di treno scelto: ";
    cin>>tipo;
    if (tipo=="a")
        suppl=biglietto*0.07;
    if (tipo=="b")
        suppl=biglietto*0.12;
    if (tipo=="c")
        suppl=biglietto*0.18;
    if (tipo=="a" || tipo=="b" || tipo=="c")
    {
        cout<<"Il treno scelto e' di tipo "<<tipo<<endl;
        cout<<"Il supplemento corrisponde a "<<suppl<<endl;
    }
    else
    {
        cout<<"Il supplemento non viene applicato"<<endl;
        suppl=0;
    }
    bigliettoTot=biglietto+suppl;
    cout<<"Il prezzo totale del biglietto e' "<<bigliettoTot;

}