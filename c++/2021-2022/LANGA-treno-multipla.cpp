//Problema: treno (selezione multipla)
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 16/11/2021
#include <iostream>
using namespace std;

main()
{
    int biglietto;
    float suppl,bigliettoTot;
    char tipo;
    cout<<"Inseririsci il prezzo del biglietto: ";
    cin>>biglietto;
    cout<<"Inseririsci il tipo di treno scelto: ";
    cin>>tipo;
    switch (tipo)
    {
    case 'a':
        suppl=biglietto*0.07;
        break;
    case 'b':
        suppl=biglietto*0.12;
        break;
    case 'c':
        suppl=biglietto*0.18;
        break;
    default:
        suppl=0;
        break;
    }
    bigliettoTot=biglietto+suppl;
    cout<<"Il treno scelto e' di tipo "<<tipo<<endl;
    cout<<"Il supplemento corrisponde a "<<suppl<<endl;
    cout<<"Il prezzo totale del biglietto e' "<<bigliettoTot;
}
