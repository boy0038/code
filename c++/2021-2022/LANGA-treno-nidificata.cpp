//Problema: treno (selezione nidificata)
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 16/11/2021

#include <iostream>
using namespace std;

main()
{
    int biglietto;
    float suppl,bigliettoTot;
    string tipo;
    cout<<"Inseririsci il prezzo del biglietto: ";
    cin>>biglietto;
    cout<<"Inseririsci il tipo di treno scelto: ";
    cin>>tipo;
    if (tipo=="a")
        suppl=biglietto*0.07;
    else
    {
        if (tipo=="b")
            suppl=biglietto*0.12;
        else
        {
            if (tipo=="c")
                suppl=biglietto*0.18;
            else
                suppl=0;   
        }
    }
    bigliettoTot=biglietto+suppl;
    cout<<"Il treno scelto e' di tipo "<<tipo<<endl;
    cout<<"Il supplemento corrisponde a "<<suppl<<endl;
    cout<<"Il prezzo totale del biglietto e' "<<bigliettoTot<<endl;
}