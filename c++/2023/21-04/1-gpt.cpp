#include <iostream>
using namespace std;

const int MAX_N = 100;

void carica_vettore(int v[], int n) 
{
    for (int i = 0; i < n; i++) 
    {
        cout << "Inserisci l'elemento " << i + 1 << ": ";
        cin >> v[i];
    }
}

void sostituisci_pari_dispari(int v[], int n) {
    for (int i = 0; i < n; i++) {
        if (v[i] % 2 == 0) {
            v[i] = 0;
        } else {
            v[i] = 1;
        }
    }
}

void stampa_vettore(int v[], int n) {
    for (int i = 0; i < n; i++) {
        cout << v[i] << " ";
    }
    cout << endl;
}

int main() {
    int v[MAX_N];
    int n;
    cout << "Quanti elementi vuoi inserire? ";
    cin >> n;
    
    carica_vettore(v, n);
    
    cout << "Vettore originale: ";
    stampa_vettore(v, n);
    
    sostituisci_pari_dispari(v, n);
    
    cout << "Vettore modificato: ";
    stampa_vettore(v, n);
    
    return 0;
}