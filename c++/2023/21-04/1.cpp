#include <iostream>
using namespace std;


void caricavet(int v[], int n)
{
    for (int i=0;i<n;i++)
    {
        cout<<"valore "<<i;
        cin>>v[i];
    }
}

void visvett(int v[], int n)
{
    for (int i=0;i<n;i++)
    {
        cout<<v[i]<<endl;
    }
}

void sostit(int v[], int n)
{
    for (int i=0;i<n;i++)
    {
        if (v[i]%2==0)
            v[i]=0;
        else
            v[i]=1;

    }
    
}

main()
{
    int n;
    cout<<"grandezza vettore:";
    cin>>n;
    int v[n];
    caricavet(v,n);
    cout<<"vettore NON modificato:"<<endl;
    visvett(v,n);
    sostit(v,n);
    cout<<"vettore modificato:"<<endl;
    visvett(v,n);

}
