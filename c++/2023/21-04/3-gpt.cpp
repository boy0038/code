#include <iostream>
using namespace std;

void caricamento(int v[], int n) {
    for (int i = 0; i < n; i++) {
        cout << "Inserisci il " << i+1 << "° elemento: ";
        cin >> v[i];
    }
}

void inverti(int v[], int n) {
    int temp;
    for (int i = 0; i < n/2; i++) {
        temp = v[i];
        v[i] = v[n-1-i];
        v[n-1-i] = temp;
    }
}

void visualizza(int v[], int n) {
    for (int i = 0; i < n; i++) {
        cout << v[i] << " ";
    }
    cout << endl;
}

int main() {
    const int SIZE = 5;
    int v[SIZE];
    
    caricamento(v, SIZE);
    visualizza(v, SIZE);
    inverti(v, SIZE);
    visualizza(v, SIZE);

    return 0;
}