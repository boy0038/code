#include <iostream>
using namespace std;


void caricavet(int v[], int n)
{
    for (int i=0;i<n;i++)
    {
        cout<<"valore "<<i;
        cin>>v[i];
    }
}

void visvett(int v[], int n)
{
    for (int i=0;i<n;i++)
    {
        cout<<v[i]<<endl;
    }
}

void invert(int v[], int n)
{
    int x;
    for (int i=0;i<n/2;i++)
    {
        x=v[i];
        v[i]=v[n-1-i];
        v[n-1-i]=x;

    }
    
}

main()
{
    int n;
    cout<<"grandezza vettore:";
    cin>>n;
    int v[n];
    caricavet(v,n);
    cout<<"vettore NON invertito:"<<endl;
    visvett(v,n);
    invert(v,n);
    cout<<"vettore invertito:"<<endl;
    visvett(v,n);

}
