//Problema:  
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 

#include <iostream>
#include <math.h>
using namespace std;

void radice(float x)
{
    cout<<"radice quadrata:"<<sqrt(x);
}

main()
{
    float x;
    cout<<"numero:";
    cin>>x;
    if (x>=0)
        radice(x);
    else
        cout<<"radice quadrata non possibile";
    
    
}