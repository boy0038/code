//Problema:  
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 

#include <iostream>
#include <math.h>
using namespace std;

void base(float x, float y)
{
    float valore=x*y;
    cout<<valore;
}
void volume(float x, float y, float z)
{
    float valore=x*y*z;
    cout<<valore;
}
void laterale(float y, float z)
{
    float valore=y*z;
    cout<<valore;
}

void risultati(float x, float y, float z)
{
    cout<<"superficie base:\n";
    base(x, y);
    cout<<"\nvolume:\n";
    volume(x, y, z);
    cout<<"\nsuperficie laterale:\n";
    laterale(y, z);
}

main()
{
    float x,y,z;
    cout<<"inserire le dimensioni x,y,z del parallelepipedo:";
    cin>>x;
    cin>>y;
    cin>>z;
    risultati(x,y,z);

}