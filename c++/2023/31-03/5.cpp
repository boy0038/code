//Problema:  
//Compilatore: Microsoft Visual Studio Code 1.61.2
//Autore: Laerti Langa
//Data di creazione: 

#include <iostream>
#include <math.h>
using namespace std;

void calcola(float x)
{
    if (0<x<100)
        cout<<"prezzo finale:"<<x*1.03+2;
    else if (100<=x<=200)
        cout<<"prezzo finale:"<<x-(x*0.1)+2;
    else if (x>200)
        cout<<"prezzo finale:"<<x-x*0.15+2;
    else
        cout<<"prezzo non valido";
}

main()
{
    float x;
    cout<<"inserire il valore:";
    cin>>x;
    calcola(x);
}
