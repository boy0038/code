//Problema: esercitazione 
//Compilatore: Microsoft Visual Studio Code 1.56.2
//Autore: Laerti Langa
//Data di creazione: 22/04/2023
#include <iostream>
using namespace std;



void puntoa(int n)
{
    for (int i = 0; i < n; i++)
    {
        cout<<"*";
    }
    cout<<""<<endl;
}

void puntob(int n)
{
    for (int i = 0; i < n; i++)
    {
        cout<<"*"<<endl;
    }
}


void puntoc(int n)

{

    cout<<"opposto del numero: "<<n*(-1)<<endl;
    cout<<"reciproco del numero: "<<1/(float)n<<endl;
}



void mostramenu(int n)
{
    char r;
    cout<<"******************* menu ********************"<<endl;
    cout<<"* a) Visualizza una riga di N asterischi    *"<<endl;
    cout<<"* b) Visualizza una colonna di N asterischi *"<<endl;
    cout<<"* c) Comunica l'opposto e il reciproco di N *"<<endl;
    cout<<"* x) Fine                                   *"<<endl;
    cout<<"*********************************************"<<endl;
    cout<<"inserire la scelta (a, b, c oppure x): ...."<<endl;
    cin>>r;
    
    switch (r)
    {
    case 'a':
        puntoa(n);
        break;
    case 'b':
        puntob(n);
        break;
    case 'c':
        puntoc(n);
        break;
    default:
        break;
    }
}


main()
{
    char r;
    do
    {
        int n;
        do
        {
            cout<<"inserire un numero intero non negativo:";
            cin>>n;
        } while (n<0);
        
        mostramenu(n);
        cout<<"ripetere il programma? (s;n)";
        cin>>r;
    } while (r=='s');
    
}