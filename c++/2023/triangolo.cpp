//Problema: triangolo 
//Compilatore: Microsoft Visual Studio Code
//Autore: Laerti Langa
//Data di creazione: 24/03/2023

#include <iostream>
#include <math.h>
using namespace std;

void ipotenusa(float b, float h)
{
	float ip;
	ip=sqrt((b*b)+(h*h));
    cout<<"ipotenusa: "<<ip<<endl;
}

void perimetro(float b, float h)
{
    float p,ip;
    ip=sqrt((b*b)+(h*h));
    p=b+h+ip;
    cout<<"perimetro:"<<p<<endl;
}

void area(float b, float h)
{
	float a;
	a=b*h/2;
	cout<<"area: "<<a;
}

main()
{
    float b,h;
    cout<<"inserire base: ";
    cin>>b;
    cout<<"inserire altezza: ";
    cin>>h;
    ipotenusa(b,h);
    perimetro(b,h);
    area(b,h);
    
}
